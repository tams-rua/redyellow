var mesDivs = []; // j'initialise mon tableau. Il est vide au départ

            var divModelBef = '<div class="float-left mr-3 mb-3 bg-'; // début du code html de mon modèle
            var divModelAf = '" style="width: 100px; height: 100px;"></div>'; // fin du code html de mon modèle


            // fonction qui génère tous le code html de mes divs
            function generateDiv() {
                var content = ""; // j'initialise une variable qui va contenir tout le code html de mes divs pour pouvoir l'insérer à la fin de la boucle

                // je parcours mon tableau mesDivs
                for(var i = 0; i < mesDivs.length; i++) {
                    var codeHtml = divModelBef + mesDivs[i] + divModelAf; // je concatène mon modèle avec la valeur du tableau selon l'index i
                    content = content + codeHtml; // je finis par concaténer le code html avec les précédents
                }

                // console.log(content);
                document.getElementById("mesDivsBlock").innerHTML = content; // enfin j'insère le code html final "content" dans mon div "mesDivsBlock"
            }

            // fonction pour rajouter "danger" dans mon tableau mesDivs
            function addDanger(){
                mesDivs.push("danger"); // j'insère à la fin de mon tableau
                console.log(mesDivs);
                generateDiv(); // je lance la génération de mes divs
            }

            // fonction pour rajouter "warning" dans mon tableau mesDivs
            function addWarning(){
                mesDivs.push("warning");
                generateDiv();
            }